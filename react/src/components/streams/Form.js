import React from 'react';
import { connect } from 'react-redux';
import { createStream } from '../../actions';

class Form extends React.Component {
  constructor(props) {
    super(props);
    this.formRef = React.createRef();
  }

  componentDidMount() {
    this.formRef.current.componentdata = {fields:[{type: 'title'}, {type: 'description'}]}
  }

  resetFields() {
    this.formRef.current.querySelector('#title').value = '';
    this.formRef.current.querySelector('#description').value = '';
  }

  onSubmit = () => {
    const title = this.formRef.current.querySelector('#title').value;
    const description = this.formRef.current.querySelector('#description').value;
    
    this.props.createStream({title, description});

    this.resetFields();
  }
  
  render() {
    return (
      <>
        <uc-form ref={this.formRef}></uc-form>
        <br/>
        <button onClick={this.onSubmit.bind(this)} className="ui button primary">Submit</button>
      </>
    )
  }
}

export default connect(null, { createStream })(Form);