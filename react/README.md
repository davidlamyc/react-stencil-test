# Routing: React-router, Reach-router 
1. React router: Not intuitive, i.e. extractedPath.contains(route), '/item' will match '/' and 'item'. Can solve this by 'exact'
2. Documentation suggests that during integration with redux, history object should be passed into action creations, which seems rather inflexible (i.e. how would you route in other parts of the application). Might have to manage history yourself
3. Reach Router: React router seems preferable as it is more maintained: https://reacttraining.com/blog/reach-react-router-future/ 

# State Management & Reactivity (Redux)
1. Steep learning curve, with fair amount of boilerplate code
2. Good tooling (Allows looking into state of redux store, playback and playforward)
3. Mature by community standards

# Compatibility with component layer
1. Was not very successful with capturing events fired from components. Could get simple cases to work, but nested components need more thought. Within component:
```javascript
import { Event, EventEmitter } from '@stencil/core';

...
export class Button {

  @Event() buttonClicked: EventEmitter;

  todoCompletedHandler(type) {
    this.buttonClicked.emit(type);
  }
}
```
Been difficult to set listeners on the react level, still work-in-progress

2. React's philosophy of one-way data flow driven DOM manipulation (i.e. re-rendering DOM when props/state changes) runs counter to vanillaJS's event-driven style. Could be looking at this too simplistically.

# Testing
1. No shadow DOM, no problems.
2. With shadow DOM, yet to test with other solutions (i.e. puppeteer, cypress)