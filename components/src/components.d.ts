/* eslint-disable */
/* tslint:disable */
/**
 * This is an autogenerated file created by the Stencil compiler.
 * It contains typing information for all components that exist in this project.
 */
import { HTMLStencilElement, JSXBase } from "@stencil/core/internal";
export namespace Components {
    interface UcForm {
        "componentdata": any;
    }
    interface UcStreamItem {
        "canmodify": boolean;
        "description": string;
        "thetitle": string;
    }
}
declare global {
    interface HTMLUcFormElement extends Components.UcForm, HTMLStencilElement {
    }
    var HTMLUcFormElement: {
        prototype: HTMLUcFormElement;
        new (): HTMLUcFormElement;
    };
    interface HTMLUcStreamItemElement extends Components.UcStreamItem, HTMLStencilElement {
    }
    var HTMLUcStreamItemElement: {
        prototype: HTMLUcStreamItemElement;
        new (): HTMLUcStreamItemElement;
    };
    interface HTMLElementTagNameMap {
        "uc-form": HTMLUcFormElement;
        "uc-stream-item": HTMLUcStreamItemElement;
    }
}
declare namespace LocalJSX {
    interface UcForm {
        "componentdata"?: any;
    }
    interface UcStreamItem {
        "canmodify"?: boolean;
        "description"?: string;
        "thetitle"?: string;
    }
    interface IntrinsicElements {
        "uc-form": UcForm;
        "uc-stream-item": UcStreamItem;
    }
}
export { LocalJSX as JSX };
declare module "@stencil/core" {
    export namespace JSX {
        interface IntrinsicElements {
            "uc-form": LocalJSX.UcForm & JSXBase.HTMLAttributes<HTMLUcFormElement>;
            "uc-stream-item": LocalJSX.UcStreamItem & JSXBase.HTMLAttributes<HTMLUcStreamItemElement>;
        }
    }
}
