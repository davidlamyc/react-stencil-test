1. Do ```npm install```, then ```npm run start``` in /server directory.
2. Do ```npm install```, then ```npm run start``` in /react directory
3. React will run on port 3000, pointing at port 3001's json server BE.

# For reference

1. Refer to ```react/src/components/streams/StreamCreate.js``` and ```react/src/components/streams/StreamList.js``` for integration of stencil components into react app.
2. Only used @Props on the components layer and React refs on the application layer to integrate for now.