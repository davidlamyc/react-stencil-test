# uc-stream-item



<!-- Auto Generated Below -->


## Properties

| Property      | Attribute     | Description | Type      | Default     |
| ------------- | ------------- | ----------- | --------- | ----------- |
| `canmodify`   | `canmodify`   |             | `boolean` | `undefined` |
| `description` | `description` |             | `string`  | `undefined` |
| `thetitle`    | `thetitle`    |             | `string`  | `undefined` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
