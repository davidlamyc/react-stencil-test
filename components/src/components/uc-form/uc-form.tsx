import { h, Component, Prop } from "@stencil/core/internal";

@Component({
  tag: 'uc-form',
  styleUrl: '../semantic-ui.css',
})
export class Input {
  // titleInput: HTMLInputElement;
  // descriptionInput: HTMLInputElement;

  // @Prop({ reflect: true, mutable: true }) thetitle;
  // @Prop({ reflect: true, mutable: true }) description;
  @Prop({ reflect: true, mutable: true }) componentdata;
  
  // @Element() el: HTMLElement;

  // @State() titleUserInput: string;
  // @State() descriptionUserInput: string;

  // ontitleInput(event: Event) {
  //   this.titleUserInput = (event.target as HTMLInputElement).value;
  //   this.thetitle = this.titleUserInput;
  // }

  // onDescriptionInput(event: Event) {
  //   this.descriptionUserInput = (event.target as HTMLInputElement).value;
  //   this.description = this.descriptionUserInput;
  // }

  render() {
    const formFields = this.componentdata.fields.map(field => 
      <div class="ui form">
      <label>{'Enter ' + field.type[0].toUpperCase() + field.type.substring(1)}</label>
      <input
        id={field.type}
      />
    </div>);

    return (
      <div>
        {formFields}
      </div>   
    )
  }
}