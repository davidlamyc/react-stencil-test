// import { Component, State, Element, Prop, Watch, Listen } from '@stencil/core';
import { h, Component, Prop } from '@stencil/core/internal';

@Component({
  tag: 'uc-stream-item',
  styleUrl: '../semantic-ui.css'
})
export class StreamItem {

  @Prop({ mutable: true, reflectToAttr: true }) thetitle: string;
  @Prop({ mutable: true, reflectToAttr: true }) description: string;
  @Prop({ mutable: true, reflectToAttr: true }) canmodify: boolean;

  render() {
    let modButtons = '';
    if (this.canmodify) {
      modButtons = <div class="right floated content">
        <button id="edit" class="ui button primary">
          Edit
        </button>
        <button id="delete" class="ui button negative">
          Delete
        </button>
      </div>
    }
    return (
      <div class="ui celled list">
        <div class="item">
        {modButtons}
          <i class="large middle aligned icon camera" />
          <div class="content">
            {this.thetitle}
            <div class="description">
              {this.description}
            </div>
          </div>
        </div>
      </div>
    )
  }
}
