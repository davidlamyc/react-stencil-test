import React from 'react';
import { connect } from 'react-redux';
import history from '../../history';

class StreamItem extends React.Component {
  constructor(props) {
    super(props);
    this.itemRef = React.createRef();
  }

  componentDidMount() {
    const button = this.itemRef.current;
    // TODO: cannot select element with custom element, to resolve
    // const editButton = button.querySelector('#edit');
    delete button.addEventListener('click', () => {
      history.push(`/streams/edit/${this.props.streamid}`)
    })
  }
  
  render() {
    return (
      <uc-stream-item
        thetitle={this.props.title}
        description={this.props.description}
        canmodify={true}
        ref={this.itemRef}
      >
      </uc-stream-item>
    )
  }
}

export default StreamItem;