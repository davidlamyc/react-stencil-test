# uc-form



<!-- Auto Generated Below -->


## Properties

| Property        | Attribute       | Description | Type  | Default     |
| --------------- | --------------- | ----------- | ----- | ----------- |
| `componentdata` | `componentdata` |             | `any` | `undefined` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
