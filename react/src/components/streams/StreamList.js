import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import { fetchStreams } from '../../actions';
import streams from '../../apis/streams';
import StreamItem from './StreamItem';

class StreamList extends React.Component {
  componentDidMount() {
    this.props.fetchStreams();
  }

  renderAdmin(stream) {
    if (stream.userId === this.props.currentUserId) {
      return (
        <div className="right floated content">
          <Link to={`/streams/edit/${stream.id}`} className="ui button primary">
            Edit
          </Link>
          <button className="ui button negative">
            Delete
          </button>
        </div>
      )
    }
  }

  renderList() {
    return this.props.streams.map(stream => {
      const canEdit = stream.userId === this.props.currentUserId;

      return (
        <StreamItem
          key={this.props.streamid}
          title={stream.title} 
          description={stream.description} 
          canmodify={canEdit}
          streamid={stream.id}
        >
        </StreamItem>
      )
    })
  }

  renderCreate() {
    return (
      <div style={{ textAlign: 'right' }}>
        <Link to="streams/new" className="ui button primary">
          Create Stream
        </Link>
      </div>
    )
  }

  render() {
    return (
      <div>
        <h2>Streams</h2>
        <div className="ui celled list">
          {this.renderList()}
        </div>
        {this.renderCreate()}
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return { 
    streams: Object.values(state.streams),
    currentUserId: state.auth.userId,
    isSignedIn: state.auth.isSignedIn
  };
}

export default connect(mapStateToProps, { fetchStreams })(StreamList);